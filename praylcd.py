#!/usr/bin/env python
from praytimes import PrayTimes
from datetime import datetime, date, timedelta
from time import sleep
from lcd_i2c import Lcd
from beeper_jack import Beeper
from backports.datetime_fromisoformat import MonkeyPatch
import time

MonkeyPatch.patch_fromisoformat()

garibaldi = (45.750720, 4.852851)
mosquee_lyon = (45.737026, 4.890229)
mosquee_paris = (48.842053, 2.355134)

def timeOf(d):
    return d.strftime('%H:%M')

def ago(d1, d2):
    seconds = (d1-d2).seconds + 59
    hours, remainder = divmod(seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    return '{}h{:02}m'.format(int(hours), int(minutes))

def salatTimes(day, pt):
    salatData = pt.getTimes(day, garibaldi, 2)
    dayDate = day.strftime('%Y-%m-%d')
    return { 
        'fajr' : datetime.fromisoformat(dayDate + ' ' + salatData['fajr']),
        'dhuhr' : datetime.fromisoformat(dayDate + ' ' + salatData['dhuhr']),
        'asr' : datetime.fromisoformat(dayDate + ' ' + salatData['asr']),
        'maghrib' : datetime.fromisoformat(dayDate + ' ' + salatData['sunset']),
        'isha' : datetime.fromisoformat(dayDate + ' ' + salatData['isha']),
    }

def computeSalat(day):
    pt = PrayTimes()
    pt.adjust({
        'fajr': 13.5,
        'asr': 'Standard',
        'isha': 14.5
        })
    pt.tune({ 
        'fajr': -5,
        'dhuhr': 5,
        'maghrib': 5})
    salat = salatTimes(day, pt)
    salat['yesterdayIsha'] = salatTimes(day - timedelta(days=-1), pt)['isha']
    salat['tomorrowFajr'] = salatTimes(day - timedelta(days=+1), pt)['fajr']
    return salat
        

def showSalat(lcd, time, nowSalatLabel, nowSalat, nextSalatLabel, nextSalat):
    #print('time for ' + nowSalatLabel + ' since ' + timeOf(nowSalat) + ', since ' + ago(time, nowSalat))
    #print('next: ' + nextSalatLabel + ' at ' + timeOf(nextSalat) + ', in ' + ago(nextSalat, time))
    lcd.lcd_string(str(time)[11:16] + '   ' + nowSalatLabel, lcd.LCD_LINE_1)
    lcd.lcd_string('>' + timeOf(nextSalat) +  ' in ' + ago(nextSalat, time), lcd.LCD_LINE_2)

def updateSalat(lcd, time, salat):
    yesterdayIsha = salat['yesterdayIsha']
    fajr = salat['fajr']
    dhuhr = salat['dhuhr']
    asr = salat['asr']
    maghrib = salat['maghrib']
    isha = salat['isha']
    tomorrowFajr = salat['tomorrowFajr']
    
    #print('Fajr: ' + timeOf(fajr) + '    Duhr: ' + timeOf(dhuhr) + '    Asr: ' + timeOf(asr) + '    Maghrib: ' + timeOf(maghrib) + '    Isha: ' + timeOf(isha))
    if (time < fajr):
        showSalat(lcd, time, 'Isha', yesterdayIsha, 'Fajr', fajr)
        return yesterdayIsha
    if time >= fajr and time < dhuhr:
        showSalat(lcd, time, 'Fajr', fajr, 'Duhr', dhuhr)
        return fajr
    if time >= dhuhr and time < asr:
        showSalat(lcd, time, 'Duhr', dhuhr, 'Asr', asr)
        return dhuhr
    if time >= asr and time < maghrib:
        showSalat(lcd, time, 'Asr', asr, 'Maghrib', maghrib)
        return asr
    if time >= maghrib and time < isha:
        showSalat(lcd, time, 'Maghrib', maghrib, 'Isha', isha)
        return maghrib
    if time >= isha:
        showSalat(lcd, time, 'Isha', isha, 'Fajr', tomorrowFajr)
        return isha

def main(beeper):
    lcd = Lcd()
    computedToday = date.today()
    salatTimes = computeSalat(computedToday)
    computedSalat = updateSalat(lcd, datetime.now(), salatTimes)
    computedTime = ''
    while True:
        now = datetime.now()
        time = timeOf(now)
        if time != computedTime:
            computedTime = time
            today = date.today()
            if (today != computedToday):
                computedToday = today
                salatTimes = computeSalat(computedToday)
            currentSalat = updateSalat(lcd, now, salatTimes)
            if (currentSalat != computedSalat):
                beeper.alarm()
                computedSalat = currentSalat
        sleep(1)

beeper = Beeper()
while True:
    try:
        main(beeper)
    except OSError:
        print('Couldn\'t update LCD, retrying in 1s')
        sleep(1)
