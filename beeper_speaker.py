import RPi.GPIO as GPIO
import time

class Beeper:
    
    def __init__(self, pin=25, freq=220):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(pin, GPIO.OUT)
        self.pwm = GPIO.PWM(pin, freq)
        self.freq=freq
        
    def __del__(self):
        GPIO.cleanup()
    
    def beep(self, duration=0.1, pause=0.1):
        self.pwm.ChangeDutyCycle(50)
        time.sleep(duration)
        self.pwm.ChangeDutyCycle(0)
        time.sleep(pause)

    def alarm(self, beeps=3, repeat=2, pause=0.3):
        self.pwm.start(50)
        for j in range(repeat):  
            for i in range(beeps):
                self.beep()
            time.sleep(pause)
        self.pwm.stop()

if __name__ == '__main__':
    b = Beeper()
    b.alarm()
