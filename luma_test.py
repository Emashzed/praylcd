#!/usr/bin/env python
import datetime
import time
from luma.core.interface.serial import i2c, spi
from luma.oled.device import sh1106
from luma.core.render import canvas
from PIL import ImageFont

serial = spi()
device = sh1106(serial)
fontLcd = ImageFont.truetype('./digital-7.ttf', 60)

def main():
    today_last_time = "Unknown"
    while True:
        now = datetime.datetime.now()
        today_time = now.strftime("%H:%M")
        if today_time != today_last_time:
            today_last_time = today_time
            with canvas(device) as draw:
                now = datetime.datetime.now()
                draw.text((0,0), today_time, font=fontLcd, fill='yellow')
        time.sleep(0.1)

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
