import RPi.GPIO as GPIO
from time import sleep

buzzer=18
GPIO.setmode(GPIO.BCM)
GPIO.setup(buzzer, GPIO.OUT)
pwm = GPIO.PWM(buzzer, 220)
pwm.start(50)
sleep(1)
pwm.stop()
GPIO.cleanup()
