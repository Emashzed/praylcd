import pigpio
from time import sleep

class Beeper:
    
    def __init__(self, pin=19, dutyCycle=500000):
        self.pin = pin
        self.dutyCycle = dutyCycle
        self.pi = pigpio.pi()
        self.pi.set_mode(pin, pigpio.OUTPUT) 
        
    def __del__(self):
        self.pi.stop()
    
    def alarm(self, repeat=3, duration=0.1, pause=0.7):
        for i in range(repeat):
            self.pi.hardware_PWM(self.pin, 220, self.dutyCycle)
            sleep(duration)
            self.pi.hardware_PWM(self.pin, 440, self.dutyCycle)
            sleep(duration)
            self.pi.hardware_PWM(self.pin, 660, self.dutyCycle)
            sleep(duration)
            self.pi.hardware_PWM(self.pin, 880, self.dutyCycle)
            sleep(duration)
            self.pi.hardware_PWM(self.pin, 0, 0)
            sleep(pause)
        #self.pi.stop()

if __name__ == '__main__':
    b = Beeper()
    b.alarm()
