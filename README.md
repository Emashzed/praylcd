# Installation packages
```
sudo apt-get install i2c-tools
sudo apt-get install python3-smbus
sudo apt-get install python3-pip
sudo apt-get install git
sudo apt-get install build-essential
sudo apt-get install python3-dev
i2cdetect -y 1
pip3 install wheel
pip3 install setuptools
pip3 install praytimes
pip3 install RPi.GPIO
pip3 install backports-datetime-fromisoformat


sudo apt-get install libfreetype6-dev
sudo apt-get install zlib1g-dev
#sudo apt-get install libjpeg8-dev zlib1g-dev
#sudo apt-get install libjpeg-dev
pip3 install luma.oled

wget https://github.com/joan2937/pigpio/archive/master.zip
unzip master.zip
cd pigpio-master
make
sudo make install

pip3 install pigpio


```
# I2C

``boot/config.txt``
```
dtparam=i2c1=on
```

```
lsmod|grep i2c
```
> i2c_bcm2708             5006  0 
```
sudo modprobe i2c-dev
lsmod|grep i2c
```
> i2c_dev                 6047  0 
> i2c_bcm2708             5006  0 

``/etc/modules``
```
i2c-dev
```

# Python 3.7

```
wget https://www.python.org/ftp/python/3.7.4/Python-3.7.4.tar.xz
tar xf Python-3.7.4.tar.xz
cd Python-3.7.4
./configure
make -j 4
sudo make altinstall
```

