import pigpio
from time import sleep

pin = 19
dutyCycle = 500000

pi = pigpio.pi()
pi.set_mode(pin, pigpio.OUTPUT) 

for i in range(3):
	pi.hardware_PWM(pin, 220, dutyCycle)
	sleep(0.1)
	pi.hardware_PWM(pin, 440, dutyCycle)
	sleep(0.1)
	pi.hardware_PWM(pin, 880, dutyCycle)
	sleep(0.1)
	pi.hardware_PWM(pin, 0, 0)
	sleep(0.7)
pi.stop()

